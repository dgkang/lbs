package goconfig

import "os"
import "io/ioutil"
import "fmt"
import "errors"
import "strings"
import "strconv"

const Sep string = " "
const Comment  = '#'

type Configure struct {
	filename string
	kv	map[string]string
	file *os.File
}

func NewConfigure(f string) (*Configure,error) {
	c := new(Configure)
	file,err := os.Open(f)
	if err != nil {
		return nil,err
	}
	c.filename = f
	c.file = file
	c.kv = make(map[string]string,1)
	return c,nil
}

func (c *Configure)ParseConfigure() error {
	defer func() {
		if c.file != nil {
			c.file.Close()
		}
	}()

	if c.file == nil {
		return errors.New("no file")
	}
	
	data,err := ioutil.ReadAll(c.file)
	if err != nil {
		return err
	}
	
	if len(data) <= 0 {
		return errors.New("empty file")
	}
	
	array := strings.Split(string(data),"\n")
	
	for i:= 0; i < len(array); i++ {
		s := strings.TrimSpace(array[i])
		if len(s) > 0 && s[0] != Comment {
			a := strings.Split(s,Sep)
			if len(a) >= 2 { 
				c.kv[strings.TrimSpace(a[0])] = strings.TrimSpace(a[1])
			}
		}
	}
	return nil
}

func (c *Configure)GetString(key string,def string) (string,error) {
	s,ok := c.kv[key]
	if ok == false {
		return def,fmt.Errorf("none \"%s\" key",key)
	}
	return s,nil;
}

func (c *Configure)GetInt(key string,def int) (int,error) {
	s,ok := c.kv[key]
	if ok == false {
		return def,fmt.Errorf("none \"%s\" key",key)
	}
	
	i,err := strconv.Atoi(s)
	if err != nil {
		return def,err
	}
	return i,nil
}