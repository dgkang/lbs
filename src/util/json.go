package util

import "encoding/json"
import "io"
import "errors"


type Geo struct{
	Id 	string
	JiepangId string
	Name	string
	Address string
	Lat		int32
	Lon		int32
	CategoryId string
	CategoryName string
	Find	bool
}

type GeoArray []*Geo

type Category struct{
	Name	string
	Icon	string
	Id		string
}

func ParseGuidFromJson(r io.Reader)(string,error){
	dec := json.NewDecoder(r)
	var root interface{}
	if e := dec.Decode(&root); e != nil {
		return "",errors.New("decode json failed")
	}
	if items,ok := root.(map[string]interface{}); ok {
		if t,o := items["guid"]; o {
			if tt,oo := t.(string); oo{
				return tt,nil
			}
		}
	}
	return "",errors.New("none \"guid\" key")
}

func ParseCreateGeoFromJson(r io.Reader)(guid string,url string,e error){
	dec := json.NewDecoder(r)
	var root interface{}
	if e := dec.Decode(&root); e != nil {
		return "","",errors.New("decode json failed")
	}
	
	if items,ok := root.(map[string]interface{}); ok {
		for k,v := range items {
			switch vv := v.(type){
				case string:
					if k == "guid" {
						guid = vv
					}else if k == "url" {
						url = vv
					}
				}
			}
	}
	return
}

func ParseGeoFromJson(r io.Reader) (GeoArray,map[string]*Category,error){
	dec := json.NewDecoder(r)
	var root interface{}
	if e := dec.Decode(&root); e != nil {
		return nil,nil,errors.New("decode json data failed")
	}
	geoArray := make(GeoArray,0)
	cats	 := make(map[string] *Category)	
	if itmes,ok := root.(map[string]interface{}); ok {		
		if a,ok := itmes["items"]; ok{			
			if array,ok := a.([]interface{}); ok{				
				for i:= 0; i < len(array); i++ {					
					if item,ok:= array[i].(map[string]interface{}); ok {						
						geo := new(Geo)	
						for k, v := range item {    	
        					switch vv := v.(type) {
        						case string:
            						if k == "name" {
            							geo.Name = vv
            						}else if k == "guid"{
            							geo.JiepangId = vv
            						}else if k == "addr"{
            							geo.Address = vv
            						}	
        						case float64:
            						if k == "lat" {
            							geo.Lat = int32(vv * 1000000)
            						}else if k == "lon"{
            							geo.Lon = int32(vv * 1000000)
            							}
            					case interface{}:
            						{
            							if aa,kk:= vv.([]interface{}); kk{
            								for _,vvv := range aa {
            									cat := new(Category)
            									if _vvv,kkk := vvv.(map[string]interface{}); kkk{
            									for _k,_v := range _vvv{
            										switch _vv := _v.(type){
            											case string:
            												if _k == "name" {
            													if len(geo.CategoryName) <= 0 {
            														geo.CategoryName = _vv
            													}
            													cat.Name = _vv
            												}else if _k == "icon" {
            													cat.Icon = _vv            											
            												}
            											}
            										}
            									}
            									cats[cat.Name] = cat
            								}
            							}
            						}
								}
							}
						geoArray = append(geoArray,geo)
					}
				}
			}
		}else{
			return nil,nil,errors.New("no \"items\" key")
		}
	}
	return geoArray,cats,nil
}
