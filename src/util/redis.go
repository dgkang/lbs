package util

import "github.com/simonz05/godis"
import "fmt"
import "io"

const L_SEARCH_LBS = "jiepang.search"
const L_ADD_LBS	    = "jiepang.add"

type redisState struct {
	proto string
	stop bool
	c *godis.Client
	l chan map[string]io.ReadWriter 	
}

var state *redisState

func ConnectRedisServer(server string,port int) error {
	state = new(redisState)
	state.proto = fmt.Sprintf("tcp:%s:%d",server,port)
	state.l = make(chan map[string]io.ReadWriter,100)
	state.c = godis.New(state.proto,0,"")
	if state.c == nil {
		return fmt.Errorf("connect %s failed",state.proto)
	}
	return nil
}

func run(){
	for !state.stop{
		r,e := state.c.Blpop([]string{L_SEARCH_LBS,L_ADD_LBS},5)
		if e != nil || r.Err != nil{
		}else {
			
		}
	}
}

