package util

import "os"
import "fmt"

const MAX_CHAN_LENGTH int = 1000

type asynLog struct{
	l chan string
	logname string
	f *os.File
	stop bool
}

var log *asynLog = new(asynLog)

func StartLog(logname string) error {
	var e error = nil
	log.l = make(chan string,MAX_CHAN_LENGTH)
	log.logname = logname
	log.f,e = os.Create(log.logname)
	if e != nil {
		return e
	}
	go func(a *asynLog){
			for !a.stop {
				s := <- a.l
				if a.f != nil {
					a.f.WriteString(s)
				}	
			}
			a.f.Close()
		}(log)
	return nil
}

func StopLog() {
	log.stop = true
}

func LogWrite(format string, a ...interface{}){
	s := fmt.Sprintf(format,a...)
	if s[len(s) - 1] != '\n' {
		sl := []byte(s)
		s =string(append(sl,'\n'))
	}
	if !log.stop {
		log.l <- s 
	}else {
		fmt.Printf("%s",s)
	}
}