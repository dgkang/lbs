package util

import "github.com/andelf/go-curl/curl"
import "io"
import "bytes"
import "fmt"


func writefunc(buf [] byte,userdata interface{}) bool {
	 if t,ok := userdata.(*bytes.Buffer); ok {
		 t.Write(buf)
	 }
	return true
}

func GetURLContent(sURL string) (io.Reader,error){
	easy := curl.EasyInit()
	defer easy.Cleanup()
	
	easy.Setopt(curl.OPT_URL,sURL)
	easy.Setopt(curl.OPT_FOLLOWLOCATION,1)
	easy.Setopt(curl.OPT_WRITEFUNCTION,writefunc)
	buf := new(bytes.Buffer)
	easy.Setopt(curl.OPT_WRITEDATA,buf)
	err := easy.Perform()
	
	code,e := easy.Getinfo(curl.INFO_RESPONSE_CODE)
	if err != nil || e != nil{
		return nil,err
	}
	var t int; var ok bool
	if t,ok = code.(int); ok && t == 200{
		return buf,nil
	}
	return nil,fmt.Errorf("response code: %d,error",t)
}