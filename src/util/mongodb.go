package util

import "labix.org/v2/mgo"
import "labix.org/v2/mgo/bson"
import "fmt"
import "time"


func MongodbTest(){
	session,err:= mgo.DialWithTimeout("localhost",time.Second * 3)

	if err != nil {
		fmt.Println("connect to server failed.")
		return
	}

	info,_:= session.BuildInfo()
	fmt.Printf("MongoDB Version:%s\nSystem Info: %s\n",info.Version,info.SysInfo)

	session.SetSafe(nil)
	c := session.DB("jiaoyin").C("place")
	e := c.Insert(bson.M{"_id": bson.ObjectIdHex("4ff8973d236b5e57ae46e65e"),"name" : "shaocheng","age" : 15})

	if e != nil {
		fmt.Printf("%s\n",e.Error())
	}
}